# Importador Masivo de Usuarios a Intranet

Web de Triathlon Catálogos: [https://intranet-importer.herokuapp.com](https://intranet-importer.herokuapp.com) 

## Recursos

- **Hosting y dominio:** [Heroku](https://www.heroku.com/)

## Stack de Tecnología

- Ruby on Rails
- MaterializeCSS
- jQuery

## Objetivo

Esta aplicación permite cargar un CSV o XLS(X) respetando un formato que está disponible para descargar y usar como plantilla.

Una vez seleccionado el archivo, la aplicación ejecuta un proceso de importación de usuarios conectándose directamente a la base de datos de la intranet a través de un API.

> ## Nota
>
> Este documento solo indica las funcionalidades programadas 
> como requerimiento de Triathlon Catálogos, si algo nuevo fue
> programado por algún colaborador de la empresa en mención,
> puede actualizar este documento.
