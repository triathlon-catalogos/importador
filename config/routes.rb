Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'importer/index'
  post 'importer', to: 'importer#create'

  root 'importer#index'
end
