require 'net/http'

class ImporterController < ApplicationController

    def index
    end

    def create
        begin
            file_uploaded = params[:file]
            file_name = Rails.root.join('tmp', 'uploads', file_uploaded.original_filename)

            # File.open(file_name, 'wb') do |file|
            #     file.write(file_uploaded.read)
            # end

            customers = get_customers(file_uploaded)
            import_users(customers)

            respond_to do |format|
                msg = { :status => 'ok', :response => 'success' }
                format.json { render :json => msg }
            end
        rescue => error
            p error
            respond_to do |format|
                msg = { :status => 500, :response => 'error' }
                format.json { render :json => msg }
            end
        end
    end

    private
    def get_customers(file_name)
        workbook = Spreadsheet.open file_name.tempfile
        worksheet = workbook.worksheet 0

        customers = []
        
        worksheet.each do |row|
            next if row[0].nil?
            nombre = row[4].to_s
            apellidos = row[2].to_s + " " + row[3].to_s
            dni = row[6].to_i.to_s
            ruc = ''
            email = row[8].to_s
            codigo = row[1].to_i.to_s

            if row[4].nil?
                nombre = row[5].to_s
            end

            if row[8].nil?
                email = dni.to_s + "@catalogothn.com.pe"
            end

            if dni.length > 8
                ruc = dni
            end

            customer = "Nombre: " + nombre + ", Apellido: " + apellidos +
                ", DNI: " + dni + ", Email: " + email + ", RUC: " +
                ruc + ", Customer No: " + codigo

            customers << {
                firstName: nombre,
                lastName: apellidos,
                email: email,
                dni: dni,
                ruc: ruc,
                customerNo: codigo
            }
        end
        
        return customers
    end

    def import_users(customers)
        uri = URI.parse('http://intranet.catalogosdeportivos.pe/import/massive')
        # header = { 'Content-Type': 'text/json' }
        data = {
            users: {
                customers: customers,
                employees: []
            }
        }

        # Create the HTTP objects
        http = Net::HTTP.new(uri.host, uri.port)
        http.read_timeout = 1800000
        request = Net::HTTP::Post.new(uri.request_uri)
        request.body = data.to_json
        p 'ready to send request'
        # Send the request
        response = http.request(request)

        p response
    end

end
