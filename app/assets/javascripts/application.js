// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require turbolinks
//= require jquery
//= require_tree .

class Importer {
    constructor() {
        $('#import-form').submit(this.importUser);
    }

    importUser(e) {
        e.preventDefault();
        let formData = new FormData();
        let fileData = $('#file-to-import').prop('files')[0];
        formData.append('file', fileData);

        $('#result-box').addClass('hide');
        $('#spinerr').addClass('hide');
        $(this).find('button').attr('disabled', true);

        $.ajax({
            url: '/importer',
            type: 'POST',
            cache: false,
            processData: false,
            contentType: false,
            data: formData,
            beforeSend(xhr) {
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
                $('#spinner').removeClass('hide');
            },
            success(res) { 
                let message = '';
                if (res.response === 'success') {
                    message = 'Los usuarios fueron importados correctamente';
                } else {
                    message = 'Un error ha ocurrido, contacte al programador.';
                    
                }
                $('#result-box').find('.result').text(message);
                $('#result-box').removeClass('hide');
                $('#spinner').addClass('hide');
                $('#import-form').find('button').removeAttr('disabled');
            },
            error(error) {
                console.error('error', error);
                let message = 'Un error ha ocurrido, contacte al programador.';
                $('#result-box').find('.result').text(message);
                $('#result-box').removeClass('hide');
                $('#spinner').addClass('hide');
                $('#import-form').find('button').removeAttr('disabled');
            }
        });
    }
};

$(document).ready(new Importer());